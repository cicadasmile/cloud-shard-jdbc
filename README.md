# cloud-shard

## 1、仓库说明

SpringCloud实现分库分表实时扩容。

## 2、分类文档

- [分库分表模式下，数据库扩容方案](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/cloud/senior/S10、分库分表扩容方案.md)
- [Shard-Jdbc分库分表，扩容方案实现](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/spring/cloud/senior/S11、分库分表扩容实现.md)

## 3、仓库整合

| 仓库 | 描述 |
|:---|:---|
| [butte-java](https://gitee.com/cicadasmile/butte-java-note) |Java编程文档整理，基础、架构，大数据 |
| [butte-frame](https://gitee.com/cicadasmile/butte-frame-parent) |微服务组件，中间件，常用功能二次封装 |
| [butte-flyer](https://gitee.com/cicadasmile/butte-flyer-parent) |butte-frame二次浅封装，实践案例 |
| [butte-auto](https://gitee.com/cicadasmile/butte-auto-parent) |Jenkins+Docker+K8S实现自动化持续集成 |
| [java-base](https://gitee.com/cicadasmile/java-base-parent) | Jvm、Java基础、Web编程，JDK源码分析 |
| [model-struct](https://gitee.com/cicadasmile/model-arithmetic-parent) | 设计模式、数据结构、算法 |
| [data-manage](https://gitee.com/cicadasmile/data-manage-parent) | 架构设计，实践，数据管理、工具 |
| [spring-mvc](https://gitee.com/cicadasmile/spring-mvc-parent) | Spring+Mvc框架基础总结 |
| [spring-boot](https://gitee.com/cicadasmile/spring-boot-base) | SpringBoot2基础，应用、配置等 |
| [middle-ware](https://gitee.com/cicadasmile/middle-ware-parent) | SpringBoot2进阶，整合常用中间件 |
| [spring-cloud](https://gitee.com/cicadasmile/spring-cloud-base) | Spring+Ali微服务基础组件用法|
| [cloud-shard](https://gitee.com/cicadasmile/cloud-shard-jdbc) | SpringCloud实现分库分表实时扩容 |
| [husky-cloud](https://gitee.com/cicadasmile/husky-spring-cloud) | SpringCloud综合入门案例 |
| [big-data](https://gitee.com/cicadasmile/big-data-parent) | Hadoop框架，大数据组件，数据服务 |
| [mysql-base](https://gitee.com/cicadasmile/mysql-data-base) | MySQL数据库基础、进阶总结 |
| [linux-system](https://gitee.com/cicadasmile/linux-system-base) | Linux系统基础，环境搭建、配置 |
